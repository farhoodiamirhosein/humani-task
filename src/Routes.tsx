import React from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import CourseSchedule from "./containers/CourseSchedule/CourseSchedule";
import CourseScheduleDetail from "./containers/CourseScheduleDetail/CourseScheduleDetail";

const AppRoutes: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route path="/course/:id/enroll" element={<CourseScheduleDetail />} />
        <Route path="/course" element={<CourseSchedule />} />
        <Route path="/" element={<CourseSchedule />} />
      </Routes>
    </Router>
  );
};

export default AppRoutes;
