import moment from "moment";

const formatClassDates = (
  timestampsArray: number[][]
): {
  days: string[];
  month: string;
  monthDays: string[];
  hours: string;
} => {
  const weekDays = [];
  const monthDays = [];
  let hours = "";
  let month = "";

  for (let i in timestampsArray) {
    const timeStartAndEnd = timestampsArray[i];
    const [start, end] = timeStartAndEnd;
    const startFormattedDay = moment.unix(start).format("dddd");
    month = moment.unix(start).format("MMMM");
    const startFormatMonth = moment.unix(start).format("d");
    const startFormattedHour = moment.unix(start).format("h:mm A");
    const endFormattedHour = moment.unix(end).format("h:mm A");
    weekDays.push(startFormattedDay);
    monthDays.push(startFormatMonth);
    hours = `${startFormattedHour} - ${endFormattedHour}`;
  }

  return { days: weekDays, month, monthDays, hours };
};

export default formatClassDates;
