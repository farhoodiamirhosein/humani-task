import classNames from "classnames";
import { ReactElement } from "react";

interface HeadingProps {
  children: string | ReactElement;
  className?: string;
  size?: "small" | "medium";
}

const sizes = {
  medium: "text-2xl",
  small: "text-xl",
};

const Heading = (props: HeadingProps) => {
  const { children, className, size = "small" } = props;

  return (
    <h3
      className={classNames(
        "text-black font-bold text-lg",
        sizes[size],
        className
      )}
    >
      {children}
    </h3>
  );
};

export default Heading;
