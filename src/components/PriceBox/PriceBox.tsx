import moment from "moment";
import Paragraph from "../Paragraph/Paragraph";

const PriceBox = ({
  price,
  availibity,
  currency,
}: {
  price: number;
  availibity: number;
  currency: string;
}) => {
  const dateObject = new Date(availibity * 1000);
  const formattedDate = moment(dateObject).format("MMMM D");

  return (
    <div className="flex items-center gap-2">
      <Paragraph className="font-bold">
        ${price} {currency}
      </Paragraph>
      <Paragraph>until {formattedDate}</Paragraph>
    </div>
  );
};

export default PriceBox;
