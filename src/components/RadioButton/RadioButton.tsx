import classNames from "classnames";
import {
  Dispatch,
  ReactElement,
  SetStateAction,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import UseFocus from "../../hooks/UseFocus";
import UseHover from "../../hooks/UseHover";

interface RadioButtonProps extends Partial<ReactElement<HTMLButtonElement>> {
  selected?: boolean;
  onChange?: (value: boolean) => void;
}

export interface RadioButtonHandlers {
  setIsHovered: Dispatch<SetStateAction<boolean>>;
}

const RadioButton = forwardRef<RadioButtonHandlers, RadioButtonProps>(
  (props, ref) => {
    const { hoverEvents, isHovered, setIsHovered } = UseHover();
    const { focusEvents, isFocused } = UseFocus();
    const { selected, onChange } = props;
    const [isActive, setIsActive] = useState(selected);

    useEffect(() => {
      setIsActive(selected);
    }, [selected]);

    const handleToggle = () => {
      if (onChange) {
        onChange(!isActive);
      }
      setIsActive(!isActive);
    };

    useImperativeHandle(ref, () => ({
      setIsHovered,
    }));

    return (
      <div
        className="relative cursor-pointer w-max h-max"
        onClick={handleToggle}
        {...hoverEvents}
      >
        <input {...focusEvents} className="opacity-0 absolute" type="radio" />
        <div
          className={classNames(
            "w-[24px] h-[24px] flex justify-center items-center rounded-lg",
            isFocused
              ? "outline-2 outline-blue-50 outline outline-offset-2"
              : ""
          )}
        >
          <div className="border-blue-50 border-solid border focus:outline-blue-50 outline-blue-50 focus:outline rounded-full w-[22px] h-[22px] flex justify-center items-center">
            <div
              className={classNames(
                "bg-blue-50 w-[12px] h-[12px] rounded-full transition-opacity ease-in-out duration-300",
                isActive || isHovered ? "opacity-1" : "opacity-0"
              )}
            />
          </div>
        </div>
      </div>
    );
  }
);

export default RadioButton;
