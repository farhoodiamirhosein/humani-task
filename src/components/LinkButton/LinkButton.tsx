import classNames from "classnames";
import { ButtonHTMLAttributes, ReactElement } from "react";

interface CustomButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: ReactElement;
}

const LinkButton = (props: CustomButtonProps) => {
  const { icon } = props;
  return (
    <button
      className={classNames(
        "border-none outline-none font-medium rounded-lg p-3 transition-background  hover:underline flex gap-3 items-center ease-in-out duration-300 focus:outline-blue-50 focus:outline-2 text-blue-50",
        props.className
      )}
      {...props}
    >
      {icon ? icon : null}
      {props.children}
    </button>
  );
};

export default LinkButton;
