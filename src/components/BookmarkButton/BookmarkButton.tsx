import classNames from "classnames";
import { ReactElement } from "react";
import FavIcon from "../icons/FavIcon";

interface BookmarkButtonProps extends Partial<ReactElement<HTMLButtonElement>> {
  selected?: boolean;
  className?: string;
  fillColor?: string;
}

const BookmarkButton = ({
  selected = false,
  className,
  fillColor,
  ...props
}: BookmarkButtonProps) => {
  return (
    <div className={classNames("cursor-pointer", className)} {...props}>
      <FavIcon fill={selected} fillColor={fillColor} />
    </div>
  );
};

export default BookmarkButton;
