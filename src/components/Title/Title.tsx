import classNames from "classnames";
import { ReactElement } from "react";

interface TitleProps {
  children: string | ReactElement | any;
  className?: string;
  size?: "small" | "large";
}

const sizes = {
  small: "text-3xl",
  large: "text-[34px]",
};

const Title = (props: TitleProps) => {
  const { children, className, size = "small" } = props;

  return (
    <h2 className={classNames("font-bold", sizes[size], className)}>
      {children}
    </h2>
  );
};

export default Title;
