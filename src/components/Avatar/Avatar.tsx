import classNames from "classnames";

interface AvatarProps {
  title: string;
  img: string;
  className?: string;
}

const Avatar = (props: AvatarProps) => {
  const { className, img, title } = props;
  return (
    <div className={classNames(className)}>
      <div className="flex md:flex-col flex-row md:item-center md:gap-0 gap-4">
        <div className="w-[80px] h-[80px] overflow-hidden rounded-full flex justify-center items-center">
          <img src={img} alt={title} />
        </div>
        <div className="pt-1 md:text-center">
          <p>Instructor:</p>
          <p>{title}</p>
        </div>
      </div>
    </div>
  );
};

export default Avatar;
