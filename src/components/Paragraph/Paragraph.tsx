import classNames from "classnames";
import { ReactElement } from "react";

interface ParagraphProps {
  children: string | ReactElement | any;
  className?: string;
}

const Paragraph = (props: ParagraphProps) => {
  const { children, className } = props;
  return (
    <p className={classNames("text-black text-lg", className)}>{children}</p>
  );
};

export default Paragraph;
