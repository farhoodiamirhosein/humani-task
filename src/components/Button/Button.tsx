import classNames from "classnames";
import { ButtonHTMLAttributes } from "react";

type CustomButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

const Button = ({ className, ...props }: CustomButtonProps) => {
  return (
    <button
      className={classNames(
        "bg-primary-60 text-white font-bold rounded-xl px-3 py-2 border-4 border-solid border-transparent hover:bg-primary-40 transition-background ease-in-out duration-300 focus:bg-primary-40 focus:border-black",
        className
      )}
      {...props}
    >
      {props.children}
    </button>
  );
};

export default Button;
