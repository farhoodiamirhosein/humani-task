import { useEffect, useRef } from "react";
import UseHover from "../../hooks/UseHover";
import formatClassDates from "../../utils/formatClassDates";
import Avatar from "../Avatar/Avatar";
import Heading from "../Heading/Heading";
import Paragraph from "../Paragraph/Paragraph";
import PriceBox from "../PriceBox/PriceBox";
import RadioButton, { RadioButtonHandlers } from "../RadioButton/RadioButton";
import Title from "../Title/Title";

interface CurseCardProps {
  timeBase: string;
  amount: number;
  valid_until: number;
  avatarImage: string;
  avatarTitle: string;
  selected?: boolean;
  currency: string;
  dates: number[][];
}

const CurseCard = (props: CurseCardProps) => {
  const { hoverEvents, isHovered } = UseHover();
  const {
    selected,
    timeBase,
    avatarImage,
    avatarTitle,
    amount,
    valid_until,
    currency,
    dates,
  } = props;

  const { days, month, monthDays, hours } = formatClassDates(dates);
  const radioRef = useRef<RadioButtonHandlers>(null);

  useEffect(() => {
    if (radioRef?.current?.setIsHovered) {
      if (isHovered) {
        radioRef.current.setIsHovered(true);
      } else {
        radioRef.current.setIsHovered(false);
      }
    }
  }, [isHovered]);

  return (
    <div
      {...hoverEvents}
      tabIndex={0}
      className="p-4 md:py-8 md:pl-6 md:pr-12 rounded-lg hover:border-black focus:border-blue-50 focus:outline-none transition-all duration-300 ease-in-out hover:shadow-none hover:border-4 cursor-pointer border-neutral-80 border-4 max-w-[100%] w-[500px]"
    >
      <div className="flex gap-3 md:flex-row flex-col justify-between">
        <div className="flex gap-3">
          <RadioButton ref={radioRef} selected={selected} />
          <div className="flex flex-col items-start pr-2">
            <Heading
              className="font-bold text-primary-30 text-start"
              size="small"
            >
              Virtual course
            </Heading>
            <Title className="font-bold text-start pt-2" size="small">
              {days.join(" & ")}, {month} {monthDays.join(" & ")}
            </Title>
            <Title className="font-normal text-start py-2" size="small">
              {hours}
            </Title>
            <Paragraph className="font-normal">{timeBase} Time</Paragraph>
            <PriceBox
              currency={currency}
              availibity={valid_until}
              price={amount}
            />
          </div>
        </div>

        <div className="md:py-6 py-2 px-8 md:px-0">
          <Avatar img={avatarImage} title={avatarTitle} />
        </div>
      </div>
    </div>
  );
};

export default CurseCard;
