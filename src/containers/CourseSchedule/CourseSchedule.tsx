import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { CourseScheduleType, getCourseSchedules } from "../../api/course";
import BookmarkButton from "../../components/BookmarkButton/BookmarkButton";
import Button from "../../components/Button/Button";
import CurseCard from "../../components/CurseCard/CurseCard";
import LinkButton from "../../components/LinkButton/LinkButton";
import useLocalStorage from "../../hooks/UseLocalstorage";
import colors from "../../styles/colors";

const fakeCourseId = "131353afdaf3452";

const CourseSchedule = () => {
  const navigate = useNavigate();
  const [courseSchedules, setCourseSchedules] = useState<CourseScheduleType[]>(
    []
  );
  const [loading, setLoading] = useState(true);
  const [selectedCourse, setSelectedCourse] = useState<number>();
  const courseId = fakeCourseId;

  const [favoriteCourses, setFavoriteCourses] = useLocalStorage<string[]>(
    "favorite-courses",
    []
  );

  const handleSelect = (id: number) => {
    setSelectedCourse(id);
  };

  const handleEnrollToCourseSchedule = () => {
    navigate(`/course/${selectedCourse}/enroll`);
  };

  const fetchData = useCallback(async () => {
    try {
      const response = await getCourseSchedules(courseId);
      setCourseSchedules(response);
    } catch (e) {
      // error handling
    }
    setLoading(false);
  }, [courseId]);

  const handleFavoriteCourse = () => {
    const newFavoriteCourses = [...favoriteCourses];
    const stringIndex = newFavoriteCourses.indexOf(courseId);
    if (stringIndex !== -1) {
      newFavoriteCourses.splice(stringIndex, 1);
    } else {
      newFavoriteCourses.push(courseId);
    }
    setFavoriteCourses(newFavoriteCourses);
  };

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <div className="flex gap-4 items-center w-100 justify-center p-6 flex-col">
      <p className="text-center py-6">
        This course fake id is <b>{fakeCourseId}</b>
      </p>
      <div className="flex gap-4 items-center flex-col max-w-full">
        {courseSchedules.length
          ? courseSchedules.map((course) => {
              return (
                <div
                  onClick={() => handleSelect(course.id)}
                  className="max-w-full"
                >
                  <CurseCard
                    avatarImage={course.instructors[0]?.portrait_image}
                    avatarTitle={course.instructors[0]?.first_name}
                    timeBase={course.location.timezone}
                    amount={course.pricing.amount}
                    valid_until={course.pricing.valid_until}
                    currency={course.pricing.currency}
                    dates={course.dates}
                    selected={course.id === selectedCourse}
                  />
                </div>
              );
            })
          : null}
        <Button
          disabled={loading || !selectedCourse}
          onClick={handleEnrollToCourseSchedule}
          className="w-full"
        >
          Enroll in course
        </Button>
        <LinkButton
          disabled={loading}
          onClick={handleFavoriteCourse}
          icon={
            <BookmarkButton
              selected={favoriteCourses.includes(courseId)}
              fillColor={colors.blue[50]}
            />
          }
        >
          Save Course
        </LinkButton>
      </div>
    </div>
  );
};
export default CourseSchedule;
