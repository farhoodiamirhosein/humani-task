import { useParams } from "react-router-dom";

const CourseScheduleDetail = () => {
  const { id } = useParams<{ id: string }>();
  return (
    <div className="py-6 text-center">
      <p>This page most show course schedule detail according to {id} id</p>
    </div>
  );
};

export default CourseScheduleDetail;
