import AppRoutes from "./Routes";
import "./styles/tailwind.css";

function App() {
  return (
    <div className="font-inter">
      <AppRoutes />
    </div>
  );
}

export default App;
