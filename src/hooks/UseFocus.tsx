import { useState } from "react";

const UseFocus = (
  onFocusChange?: (value: boolean) => void,
  defaultValue?: boolean
) => {
  const [isFocused, setIsFocused] = useState(defaultValue || false);

  const onFocus = () => {
    setIsFocused(true);
    if (onFocusChange) onFocusChange(true);
  };
  const onBlur = () => {
    setIsFocused(false);
    if (onFocusChange) onFocusChange(false);
  };

  return {
    focusEvents: { onFocus, onBlur },
    isFocused,
    setIsFocused,
  };
};

export default UseFocus;
