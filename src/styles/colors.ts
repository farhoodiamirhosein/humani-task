const colors = {
  primary: {
    20: "#4D0001",
    30: "#600C20",
    40: "#A61518",
    60: "#ED0000",
  },
  neutral: {
    0: "#000000",
    80: "#CCCCCC",
    100: "#FFFFFF",
  },
  blue: {
    50: "#0073DD",
  },
};

export default colors;
